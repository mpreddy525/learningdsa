public class Solution {
    public int FindNumbers(int[] nums) {
        int count = 0;
        
        foreach(int num in nums){
            if(GetDigitsCount(num)%2 == 0)
                count++;
        }
        
        return count;
    }
    
    public int GetDigitsCount(int num)
    {
        int count = 0;
         while(num > 0){
             num /= 10;
             count++;
         }
        return count;
    }
}