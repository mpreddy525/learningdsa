public class Solution {
    public int FindMaxConsecutiveOnes(int[] nums) {
        int maxConsecutiveOnes = 0;
        int consecutiveOnesCount = 0;
        
        foreach(int digit in nums)
        {
            if(digit == 1)
                consecutiveOnesCount++;
            
            if(maxConsecutiveOnes < consecutiveOnesCount)
                maxConsecutiveOnes = consecutiveOnesCount;
            
            if(digit == 0)
                consecutiveOnesCount = 0;
        }
        
        return maxConsecutiveOnes;
    }
}